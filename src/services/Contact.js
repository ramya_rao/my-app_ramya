import axios from 'axios';
import handleError from './ErrorHandler';

import {
    API_ENDPOINT,
    CONTACTS_ENDPOINT,
    HEADERS
} from './Constants';

export default class Contact {
  queryContacts = async () => axios({
    method: 'get',
    url: `${API_ENDPOINT}${CONTACTS_ENDPOINT}`
  })
    .then(response => response.data)
    .catch(error => handleError(error))


  registerContact = async (params) => axios({
    method: 'post',
    headers: HEADERS(),
    data: params,
    url: `${API_ENDPOINT}${CONTACTS_ENDPOINT}`
  })
    .then(response => response.data)
    .catch(error => handleError(error))
}