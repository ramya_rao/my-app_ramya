export const API_ENDPOINT = 'http://localhost:5641';
export const CONTACTS_ENDPOINT = '/contacts';
export const HEADERS = () => new Headers({ 'content-type': 'application/json'});