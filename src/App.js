import React from 'react';
import {
  Route,
  Link,
  BrowserRouter as Router, 
  Switch
} from 'react-router-dom';

import Home from './Home';
import Contacts from './Contacts';
import AddContact from './AddContact';

const App = () => (
  <Router>
    <React.Fragment>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/contacts">Users</Link>
        </li>
        <li>
          <Link to="/addContact">Add a new Phone Number</Link>
        </li>
      </ul>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/contacts" component={Contacts}/>
        <Route exact path="/addContact" component={AddContact}/>
        <Route default component={Home} />
      </Switch>
    </React.Fragment>
  </Router>
)

export default App;
