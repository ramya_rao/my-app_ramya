import React, {Component} from 'react';
import Contact from './services/Contact'

const contacts = [{
    id: '1',
    name: 'Jane Doe',
    contact: '9990999999'
},{
    id: '2',
    name: 'John Doe',
    contact: '9990999999'
}]

// const Contacts = ({contacts}) => (
//     <div className="container">
//         <table className="table table-bordered table-striped">
//         <thead>
//             <tr>
//                 <th >#</th>
//                 <th >Name</th>
//                 <th >Contact</th>
//             </tr>
//         </thead>
//         <tbody>
//             {contacts.map(contact => (
//                 <tr>
//                 <th scope="col">{contact.id}</th>
//                 <td>{contact.name}</td>
//                 <td>{contact.contact}</td>
//                 </tr>
//             ))}
//         </tbody>
//         </table>
//     </div>
// );

class Contacts extends Component {
    contact = new Contact();
   constructor(props) {
       super(props);
       this.state = {
           contacts: []
       }
       this.queryContacts = this.queryContacts.bind(this);
   }

   componentDidMount() {
      this.queryContacts();
   }

   async queryContacts() {
       try {
          const response = await this.contact.queryContacts();
          this.setState({contacts: response});
          console.log(response);
       } catch (err) {
           console.log(err)
       }
   }

   render() {
       const {contacts} = this.state
       return (
        <React.Fragment>
        {contacts.length > 0 && (
        <div className="container">
            <table className="table table-bordered table-striped">
            <thead>
                <tr>
                    <th >#</th>
                    <th >Name</th>
                    <th >Contact</th>
                </tr>
            </thead>
            <tbody>
                {contacts.map(contact => (
                    <tr>
                    <th scope="col">{contact.id}</th>
                    <td>{contact.name}</td>
                    <td>{contact.contact}</td>
                    </tr>
                ))}
            </tbody>
            </table>
        </div>
        )}
        </React.Fragment> 
       )
   }
}

export default Contacts;